package sincronizador;

import java.util.List;

import modelo.Campania;
import modelo.Contesta;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Tematica;
import modelo.Ubicacion;

public interface Conexion {
    void obtenerCampanias(List<Campania> campanias);
    void obtenerUbicaciones(List<Ubicacion> ubicaciones);
    void obtenerRespuestas(List<Respuesta> respuestas);
    void obtenerTematicas(List<Tematica> tematicas);
    void obtenerPreguntas(List<Pregunta> preguntas);
    void actualizarContesta(List<Contesta> contesta);
}
