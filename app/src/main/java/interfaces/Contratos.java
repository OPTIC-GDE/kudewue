package interfaces;


public class Contratos {

    public interface Tablas {
        String PARTICIPANTE = "participante";
        String CAMPANIA = "campania";
        String DISPOSITIVO = "dispositivo";
        String UBICACION = "ubicacion";
        String CAMPANIA_PREGUNTA = "campaniaPregunta";
        String TEMATICA = "tematica";
        String PREGUNTA = "pregunta";
        String RESPUESTA = "respuesta";
        String CONTESTA = "contesta";
    }

    interface ColumnasParticipante{
        String ID_PARTICIPANTE = "parId";
        String ID_CAMPANIA = "camId";
        String ID_UBICACION = "ubiId";
        String ID_DISPOSITIVO = "disId";
        String ES_ACTIVO = "parEsActivo";
        String EDAD = "parEdad";
    }
    interface ColumnasCampania{
        String ID_CAMPANIA = "camId";
        String NOMBRE = "camNombre";
        String DESCRIPCION = "camDescripcion";
        String ES_ACTIVA = "camActiva";

    }
    interface ColumnasDispositivo{
        String ID_DISPOSITIVO = "disId";
    }
    interface ColumnasUbicacion{
        String ID_UBICACION = "ubiId";
        String UBICACION = "ubiNombre";
    }
    interface ColumnasTematica{
        String ID_TEMATICA = "temId";
        String NOMBRE = "temNombre";
    }
    interface ColumnasPregunta{
        String ID_PREGUNTA = "preId";
        String ID_TEMATICA = "temId";
        String PREGUNTA = "preTexto";

    }
    interface ColumnasRespuesta{
        String ID_RESPUESTA = "resId";
        String RESPUESTA = "resTexto";
        String ES_CORRECTA = "resEsCorrecta";
        String ID_PREGUNTA = "preId";
    }
    interface ColumnasContesta{
        String ID_CONTESTA = "conId";
        String ID_DISPOSITIVO="disId";
        String ID_PARTICIPANTE = "parId";
        String ID_RESPUESTA = "resId";
        String FECHA = "conFecha";
    }
    interface ColumnasCampaniaPregunta{
        String ID_CAMPANIA = "camId";
        String ID_PEGUNTA = "preId";
    }

    public interface Referencia{
        String ID_RESPUESTA = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.RESPUESTA, Respuestas.ID_RESPUESTA);
        String ID_PREGUNTA = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.PREGUNTA, Preguntas.ID_PREGUNTA);
        String ID_TEMATICA = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.TEMATICA, Tematicas.ID_TEMATICA);
        String ID_DISPOSITIVO = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.DISPOSITIVO, Dispositivos.ID_DISPOSITIVO);
        String ID_PARTICIPANTE = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.PARTICIPANTE, Participantes.ID_PARTICIPANTE);
        String ID_UBICACION = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.UBICACION, Ubicaciones.ID_UBICACION);
        String ID_CONTESTA = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.CONTESTA, Contestan.ID_CONTESTA);
        String ID_CAMPANIA = String.format("REFERENCES %s(%s) ON UPDATE CASCADE ON DELETE CASCADE",
                Tablas.CAMPANIA, Campanias.ID_CAMPANIA);
    }
    public static class Tematicas implements ColumnasTematica{

    }
    public static class Preguntas implements ColumnasPregunta{

    }
    public static class Respuestas implements ColumnasRespuesta{

    }
    public static class Participantes implements ColumnasParticipante{

    }
    public static class Campanias implements ColumnasCampania{

    }
    public static class Dispositivos implements ColumnasDispositivo{

    }
    public static class Ubicaciones implements ColumnasUbicacion{

    }
    public static class Contestan implements ColumnasContesta{

    }
    public static class CampaniasPreguntas implements ColumnasCampaniaPregunta{

    }

}
