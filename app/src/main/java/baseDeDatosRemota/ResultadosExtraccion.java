package baseDeDatosRemota;

import java.util.LinkedList;

import ModeloAux.CampaniaPregunta;
import modelo.Campania;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Tematica;
import modelo.Ubicacion;

public class ResultadosExtraccion {
    private LinkedList<Campania> campanias;
    private LinkedList<Pregunta> preguntas;
    private LinkedList<CampaniaPregunta> campaniaPreguntas;
    private LinkedList<Tematica> tematicas;
    private LinkedList<Ubicacion> ubicaciones;
    private LinkedList<Respuesta> respuestas;

    public ResultadosExtraccion(LinkedList<Campania> campanias, LinkedList<Pregunta> preguntas, LinkedList<CampaniaPregunta> campaniaPreguntas) {
        this.campanias = campanias;
        this.preguntas = preguntas;
        this.campaniaPreguntas = campaniaPreguntas;
    }

    public ResultadosExtraccion() {
    }

    public LinkedList<Campania> getCampanias() {
        return campanias;
    }

    public void setCampanias(LinkedList<Campania> campanias) {
        this.campanias = campanias;
    }

    public LinkedList<Pregunta> getPreguntas() {
        return preguntas;
    }

    public void setPreguntas(LinkedList<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public LinkedList<CampaniaPregunta> getCampaniaPreguntas() {
        return campaniaPreguntas;
    }

    public void setCampaniaPreguntas(LinkedList<CampaniaPregunta> campaniaPreguntas) {
        this.campaniaPreguntas = campaniaPreguntas;
    }

    public LinkedList<Tematica> getTematicas() {
        return tematicas;
    }

    public void setTematicas(LinkedList<Tematica> tematicas) {
        this.tematicas = tematicas;
    }

    public LinkedList<Ubicacion> getUbicaciones() {
        return ubicaciones;
    }

    public void setUbicaciones(LinkedList<Ubicacion> ubicaciones) {
        this.ubicaciones = ubicaciones;
    }

    public void setRespuestas(LinkedList<Respuesta> respuestas){
        this.respuestas = respuestas;

    }
    public LinkedList<Respuesta> getRespuestas(){
        return this.respuestas;

    }
}
