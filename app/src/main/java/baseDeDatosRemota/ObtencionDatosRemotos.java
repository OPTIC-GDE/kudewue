package baseDeDatosRemota;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.LinkedList;

import ModeloAux.CampaniaPregunta;
import interfaces.Contratos;
import interfaces.Contratos.Tablas;
import modelo.Campania;
import modelo.Contesta;
import modelo.Dispositivo;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Tematica;
import modelo.Ubicacion;
import utiles.OnTaskComplete;

public class ObtencionDatosRemotos extends AsyncTask<Void, Void, Boolean> {
    private ResultadosExtraccion datosBDRemota;
    private OnTaskComplete listener;
    private Context context;

    public ObtencionDatosRemotos(OnTaskComplete listener,Context context) {
        datosBDRemota = new ResultadosExtraccion();
        this.listener = listener;
        this.context = context;
    }

    public ResultadosExtraccion obtenerInfo(){
        execute();
        return datosBDRemota;

    }

    private boolean obtener() {
        Connection conexion = null;
        boolean retorno = false;
        Log.e("Comenzando proceso","sdasdasdsd");

        try {
            conexion = Conexion.conectar();
            if (conexion == null) {
                Log.e("La conexion es null", "La conexion es null");
            } else {
                obtenerPreguntas(conexion);
                obtenerCampaniaPregunta(conexion);
                obtenerCampanias(conexion);
                obtenerUbicacion(conexion);
                obtenerTematica(conexion);
                obtenerRespuestas(conexion);
                //
                //TERMINAR DE SINCRONIZAR EL RESTO DE LOS DATOS!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                //
                Dispositivo dispositivo = Dispositivo.obtener(this.context);
                Log.e("Se obtuvo Dispositivo",  String.valueOf(dispositivo.getIdDispositivo()));

                LinkedList<Participante> listaParticipante = Participante.obtenerTodos(this.context);
               if(!listaParticipante.isEmpty()){
                    Log.e("Se obtuvo Participante",  String.valueOf(listaParticipante.get(0).getIdParticipante()));

                }else{
                   Log.e("Se obtuvo Participante",  "puta");

                }

                Log.e("Se obtuvo Participante",  "puta2");

                LinkedList<Contesta> listaContesta = Contesta.obtenerTodos(this.context);
                if(!listaContesta.isEmpty()){
                    Log.e("Se obtuvo contesta",  String.valueOf(listaContesta.get(0).getIdContesta()));

                }else{
                    Log.e("Se obtuvo Participante",  "puta3");

                }
                insertarDispositivo(conexion,dispositivo);
                Log.e("in remota","1");

                insertarParticipante(conexion,listaParticipante,dispositivo);
                Log.e("in remota","2");

                insertarContestan(conexion,listaContesta,dispositivo);
                Log.e("in remota","3");

                boolean cam = Campania.sincronizarCampania(this.datosBDRemota.getCampanias(),this.context);
                boolean preg = Pregunta.sincronizarPreguntas(this.datosBDRemota.getPreguntas(),this.context);
                boolean campaniaPregunta = CampaniaPregunta.sincronizarCampaniaPregunta(this.datosBDRemota.getCampaniaPreguntas(),this.context);
                boolean ubi = Ubicacion.sincronizarUbicaciones(this.datosBDRemota.getUbicaciones(),this.context);
                boolean tem = Tematica.sincronizarTematicas(this.datosBDRemota.getTematicas(),this.context);
                boolean res = Respuesta.sincronizarRespuesta(this.datosBDRemota.getRespuestas(),this.context);
                Log.e("sincroniza","inserta de manera local");


                if(!cam){
                    Log.e("campania", "No pudo sincronizar campania");
                }
                if(!preg){
                    Log.e("Pregunta", "No pudo sincronizar pregunta");
                }
                if(!campaniaPregunta){
                    Log.e("campaniaPregunta", "No pudo sincronizar campaniaPregunta");
                }
                if(!ubi){
                    Log.e("ubicacion", "No pudo sincronizar ubicacion");
                }
                if(!tem){
                    Log.e("Tematica", "No pudo sincronizar tematica");
                }
                if(!res){
                    Log.e("respuesta", "No pudo sincronizar respuesta");
                }

                retorno = cam && preg && campaniaPregunta && ubi && tem && res;
            }
        } catch (SQLException | ClassNotFoundException e) {

            Log.e("La conexion es null", e.getStackTrace().toString());

            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        } finally {
            try {
                conexion.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return retorno;


    }

    private void insertarContestan(Connection conexion, LinkedList<Contesta> listaContesta,Dispositivo dis) throws SQLException, ParseException {
        for(Contesta contesta: listaContesta){
            Contesta.guardarBaseRemota(contesta,conexion,dis);
        }
    }

    private void insertarParticipante(Connection conexion, LinkedList<Participante> listaParticipante ,Dispositivo dis) throws SQLException {
        for(Participante participante: listaParticipante){
            Participante.guardarBaseRemota(participante,conexion,dis);
        }
    }
    private void insertarDispositivo(Connection conexion, Dispositivo dis) throws SQLException {
        Dispositivo.guardatBaseRemota(dis,conexion);
    }

    private void obtenerCampaniaPregunta(Connection conexion) throws SQLException {
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        String consulta = "SELECT * FROM "+ Tablas.CAMPANIA_PREGUNTA;
        resultSet = st.executeQuery(consulta);
        datosBDRemota.setCampaniaPreguntas(CampaniaPregunta.obtenerCampaniaPregunta(resultSet));
    }

    private void obtenerPreguntas(Connection conexion) throws SQLException {
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        String consulta = "SELECT * FROM "+ Contratos.Tablas.PREGUNTA;
        resultSet = st.executeQuery(consulta);
        datosBDRemota.setPreguntas(Pregunta.obtenerPreguntasAux(resultSet));

    }


    private void obtenerCampanias(Connection conexion) throws SQLException{
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        resultSet = st.executeQuery("SELECT * FROM " + Tablas.CAMPANIA);
        datosBDRemota.setCampanias(Campania.armarCampaniasResulSet(resultSet));
    }

    private void obtenerTematica(Connection conexion) throws SQLException{
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        resultSet = st.executeQuery("SELECT * FROM " + Tablas.TEMATICA);
        datosBDRemota.setTematicas(Tematica.armarTematicaResultSet(resultSet));
    }
    private void obtenerRespuestas(Connection conexion) throws SQLException{
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        resultSet = st.executeQuery("SELECT * FROM " + Tablas.RESPUESTA);
        datosBDRemota.setRespuestas(Respuesta.armarRespuestaResultSet(resultSet));
    }

    private void obtenerUbicacion(Connection conexion) throws SQLException{
        Statement st;
        st = conexion.createStatement();
        java.sql.ResultSet resultSet;
        resultSet = st.executeQuery("SELECT * FROM " + Tablas.UBICACION);
        datosBDRemota.setUbicaciones(Ubicacion.armarUbicacionResultSet(resultSet));
    }

    private void actualizarBaseLocal(){

    }

    @Override
    protected Boolean doInBackground(Void... voids) {
        boolean retorno = obtener();
        return new Boolean(retorno);
    }

    @Override
    protected void onPostExecute(Boolean result){
        listener.onTaskCompleted(result.booleanValue());
    }
}
