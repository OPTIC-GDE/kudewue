package modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;
import android.widget.Toast;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;

import interfaces.Contratos.*;
import sqlite.OperacionesBD;

public class Campania implements Serializable {
    private long idCampania;
    private String nombre;
    private String descripcion;
    private boolean activa = false;
    private List<Pregunta> preguntas;

    public Campania(long idCampania, String nombre, String descripcion, boolean activa, List<Pregunta> preguntas) {
        this.idCampania = idCampania;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.activa = activa;
        this.preguntas = preguntas;
    }

    public Campania(long idCampania, String nombre, String descripcion) {
        this.idCampania = idCampania;
        this.nombre = nombre;
        this.descripcion = descripcion;
    }

    public Campania(String nombre, boolean activa) {
        this.nombre = nombre;
        this.activa = activa;
    }

    @Override
    public String toString(){
        return this.nombre.trim();
    }

    public Campania(long idCampania) {
        this.idCampania = idCampania;
    }

    public long getIdCampania() {
        return idCampania;
    }

    public void setIdCampania(long idCampania) {
        this.idCampania = idCampania;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public boolean isActiva() {
        return activa;
    }

    public void setActiva(boolean activa) {
        this.activa = activa;
    }

    public List<Pregunta> getPreguntas(Context contexto) {
        if(preguntas == null  || preguntas.isEmpty()){
            preguntas = obtenerPreguntasBD(contexto);
        }
        return preguntas;
    }
    public List<Pregunta> getPreguntas() {
        return preguntas;
    }

    public LinkedList<Pregunta> obtenerPreguntasBD(Context contexto){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(contexto);
        Cursor cursorPregunta = datos.obtenerCampaniaPreguntas(this);
        LinkedList<Pregunta> resultado = obtenerPreguntasAux(cursorPregunta);
        cursorPregunta.close();
        return resultado;

    }

    private LinkedList<Pregunta> obtenerPreguntasAux(Cursor cur){
        LinkedList<Pregunta> preguntas = new LinkedList<>();
        while(cur != null && cur.moveToNext()){
            Tematica tematica = new Tematica(cur.getLong(cur.getColumnIndex(Tematicas.ID_TEMATICA)));
            tematica.setTematica(cur.getString(cur.getColumnIndex(Tematicas.NOMBRE)));
            Pregunta pregunta = new Pregunta(cur.getLong(cur.getColumnIndex(Preguntas.ID_PREGUNTA)));
            pregunta.setPregunta(cur.getString(cur.getColumnIndex(Preguntas.PREGUNTA)));
            pregunta.setTematica(tematica);
            preguntas.add(pregunta);
        }
        return preguntas;

    }

    public Set<Tematica> obtenerTematicas(){
        Set<Tematica> set = new HashSet<>();
        for (Pregunta pregunta: this.preguntas) {
            set.add(pregunta.getTematica());
        }
        return set;
    }

    public void setPreguntas(List<Pregunta> preguntas) {
        this.preguntas = preguntas;
    }

    public static LinkedList<Campania> obtener(Context contexto){
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        Cursor cursorCampanias = datos.obtenerCampaniasActivas();
        LinkedList result = armarCampanias(cursorCampanias);
        cursorCampanias.close();
        return result;
    }

    private static LinkedList<Campania> armarCampanias(Cursor cur){
        LinkedList<Campania> list = new LinkedList<>();
        while (cur.moveToNext()){
            Campania campania = new Campania(cur.getLong(cur.getColumnIndex(Campanias.ID_CAMPANIA)));
            campania.setNombre(cur.getString(cur.getColumnIndex(Campanias.NOMBRE)));
            campania.setDescripcion(cur.getString(cur.getColumnIndex(Campanias.DESCRIPCION)));
            campania.setActiva(true);
            list.add(campania);
        }
        return list;
    }

    public static LinkedList<Campania> armarCampaniasResulSet(ResultSet cur){
        LinkedList<Campania> list = new LinkedList<>();
        try {
            while (cur.next()) {
                Campania campania = new Campania(cur.getLong(Campanias.ID_CAMPANIA));
                campania.setNombre(cur.getString(Campanias.NOMBRE));
                campania.setDescripcion(cur.getString(Campanias.DESCRIPCION));
                boolean activo = cur.getBoolean(Campanias.ES_ACTIVA);
                campania.setActiva(activo);
                list.add(campania);
                Log.e("campania",campania.getNombre());

            }
        }catch (SQLException e){
            Log.e("error","srsdser");
            e.printStackTrace();
        }
        return list;
    }

    public static boolean sincronizarCampania(List<Campania> campanias, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (Campania campania : campanias) {
                datos.actualizarCampania(campania);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

    public boolean persisitirOActualizar(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try{
            datos.actualizarCampania(this);
            datos.getWBD().setTransactionSuccessful();
        }catch (SQLiteException e){
            exito = false;
        }finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

}
