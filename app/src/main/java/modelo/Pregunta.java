package modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import interfaces.Contratos;
import interfaces.Contratos.Respuestas;
import sqlite.OperacionesBD;

public class Pregunta implements Serializable {
    private long idPregunta;
    private String pregunta;
    private Tematica tematica;

    public Pregunta(long idPregunta, String pregunta) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
    }

    public Pregunta(long idPregunta, String pregunta, Tematica tematica) {
        this.idPregunta = idPregunta;
        this.pregunta = pregunta;
        this.tematica = tematica;
    }

    public Tematica getTematica() {
        return tematica;
    }

    public void setTematica(Tematica tematica) {
        this.tematica = tematica;
    }

    public Pregunta(long idPregunta) {
        this.idPregunta = idPregunta;
    }

    public boolean persisitirOActualizar(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try{
            datos.actualizarPregunta(this);
            datos.getWBD().setTransactionSuccessful();
        }catch (SQLiteException e){
            exito = false;
        }finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

    public long getIdPregunta() {
        return idPregunta;
    }

    public void setIdPregunta(long idPregunta) {
        this.idPregunta = idPregunta;
    }

    public LinkedList<Respuesta> obtenerRespuestas(Context context){
        return Respuesta.obtenerRespuestas(context,this);
    }

    public String getPregunta() {
        return pregunta;
    }

    public void setPregunta(String pregunta) {
        this.pregunta = pregunta;
    }

    public static LinkedList<Pregunta> obtenerPreguntasAux(ResultSet cur) throws SQLException {
        LinkedList<Pregunta> preguntas = new LinkedList<>();
        while(cur != null && cur.next()){
            Tematica tematica = new Tematica(cur.getLong(Contratos.Tematicas.ID_TEMATICA));
            Pregunta pregunta = new Pregunta(cur.getLong(Contratos.Preguntas.ID_PREGUNTA));
            pregunta.setPregunta(cur.getString(Contratos.Preguntas.PREGUNTA));
            pregunta.setTematica(tematica);
            preguntas.add(pregunta);
            Log.e("Pregunta",pregunta.getPregunta());
        }
        return preguntas;

    }

    public static boolean sincronizarPreguntas(List<Pregunta> preguntas, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (Pregunta pregunta : preguntas) {
                datos.actualizarPregunta(pregunta);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

}
