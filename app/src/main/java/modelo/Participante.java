package modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.provider.Telephony;
import android.util.Log;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.LinkedList;

import interfaces.Contratos;
import interfaces.Contratos.*;
import sqlite.OperacionesBD;

public class Participante implements Serializable {
    private long idParticipante;
    private Campania campania;
    private Dispositivo dispositivo;
    private Ubicacion ubicacion;
    private boolean esActivo;
    private String edad;

    public Participante(long idParticipante, Campania campania, Dispositivo dispositivo, Ubicacion ubicacion, boolean esActivo, String edad) {
        this.idParticipante = idParticipante;
        this.campania = campania;
        this.dispositivo = dispositivo;
        this.ubicacion = ubicacion;
        this.esActivo = esActivo;
        this.edad = edad;
    }

    public Participante(Campania campania, Dispositivo dispositivo, Ubicacion ubicacion, boolean esActivo, String edad) {
        this.campania = campania;
        this.dispositivo = dispositivo;
        this.ubicacion = ubicacion;
        this.esActivo = esActivo;
        this.edad = edad;
    }

    public Participante(long idParticipante, Dispositivo dispositivo, boolean esActivo, String edad) {
        this.idParticipante = idParticipante;
        this.dispositivo = dispositivo;
        this.esActivo = esActivo;
        this.edad = edad;
    }

    public Participante(long idParticipante) {
        this.idParticipante = idParticipante;
    }

    public Participante() {

    }

    public long getIdParticipante() {
        return idParticipante;
    }

    public void setIdParticipante(long idParticipante) {
        this.idParticipante = idParticipante;
    }

    public Campania getCampania() {
        return campania;
    }

    public void setCampania(Campania campania) {
        this.campania = campania;
    }

    public Dispositivo getDispositivo() {
        return dispositivo;
    }

    public void setDispositivo(Dispositivo dispositivo) {
        this.dispositivo = dispositivo;
    }

    public Ubicacion getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(Ubicacion ubicacion) {
        this.ubicacion = ubicacion;
    }

    public boolean isEsActivo() {
        return esActivo;
    }

    public void setEsActivo(boolean esActivo) {
        this.esActivo = esActivo;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public boolean persistir(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try{
            long id = datos.agregarParticipante(this);
            Log.e("idParticipante2",String.valueOf(id));
            this.setIdParticipante(id);
            datos.getWBD().setTransactionSuccessful();
        }catch (SQLiteException e){
            exito =  false;
        }finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

    public static Participante obtenerActivo(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        Cursor cur = datos.obtenerParticipanteActivo();
        return armarParticipante(cur);
    }

    public static LinkedList<Participante> obtenerTodos(Context context) {
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        datos.getRBD().beginTransaction();
        LinkedList<Participante> retorno;
        try {
            Cursor cur = datos.obtenerParticipantes();
            retorno = armarParticipanteAux(cur);
        } catch (Exception e) {
            retorno = new LinkedList<Participante>();

        } finally {
            datos.getRBD().endTransaction();
        }
        return retorno;
    }

    private static LinkedList<Participante> armarParticipanteAux(Cursor cur){

        String[] proyeccion = {
                Participantes.ID_PARTICIPANTE,
                Participantes.EDAD,
                Participantes.ID_UBICACION,
                Participantes.ID_DISPOSITIVO,
                Participantes.ES_ACTIVO,
                Participantes.ID_CAMPANIA
        };

        LinkedList<Participante> participantes = new LinkedList<>();
        while (cur.moveToNext()) {
            Participante participante;
            Ubicacion ubicacion;
            Campania campania;

            ubicacion = new Ubicacion(cur.getLong(cur.getColumnIndex(Participantes.ID_UBICACION)));
            Dispositivo dispositivo = new Dispositivo(cur.getLong(cur.getColumnIndex(Participantes.ID_DISPOSITIVO)));

            campania = new Campania(cur.getLong(cur.getColumnIndex(Participantes.ID_CAMPANIA)));

            participante = new Participante(cur.getLong(cur.getColumnIndex(Participantes.ID_PARTICIPANTE)));
            participante.setCampania(campania);

            participante.setUbicacion(ubicacion);
            participante.setDispositivo(dispositivo);
            participante.setEdad(cur.getString(cur.getColumnIndex(Participantes.EDAD)));

            participante.setEsActivo(true);
            participantes.add(participante);
        }
        return participantes;
    }

    private static Participante armarParticipante(Cursor cur){
        Participante participante = null;
        if(cur != null && cur.moveToNext()){
            Dispositivo dispositivo = new Dispositivo(cur.getLong(cur.getColumnIndex(Dispositivos.ID_DISPOSITIVO)));
            Campania campania = new Campania(cur.getLong(cur.getColumnIndex(Campanias.ID_CAMPANIA)));
            campania.setDescripcion(cur.getString(cur.getColumnIndex(Campanias.DESCRIPCION)));
            campania.setNombre(cur.getString(cur.getColumnIndex(Campanias.NOMBRE)));
            campania.setActiva(true);
            participante = new Participante(cur.getLong(cur.getColumnIndex(Participantes.ID_PARTICIPANTE)));
            participante.setCampania(campania);
            participante.setDispositivo(dispositivo);
            participante.setEdad(cur.getString(cur.getColumnIndex(Participantes.EDAD)));
            participante.setEsActivo(true);
        }
        return participante;

    }

    public static boolean guardarBaseRemota(Participante participante, Connection conexion, Dispositivo dispositivo) throws SQLException {
        PreparedStatement st;
        String query = String.format("INSERT INTO %s ( %s, %s, %s, %s,%s) SELECT ?,?,?,?,? WHERE NOT EXISTS " +
                        "(SELECT ( %s, %s, %s, %s,%s) FROM %s where %s = ? AND %s = ? )",
                Tablas.PARTICIPANTE,
                Participantes.ID_PARTICIPANTE,
                Participantes.EDAD,
                Participantes.ID_CAMPANIA,
                Participantes.ID_UBICACION,
                Participantes.ID_DISPOSITIVO,
                Participantes.ID_PARTICIPANTE,
                Participantes.EDAD,
                Participantes.ID_CAMPANIA,
                Participantes.ID_UBICACION,
                Participantes.ID_DISPOSITIVO,
                Tablas.PARTICIPANTE,
                Participantes.ID_PARTICIPANTE,
                Participantes.ID_DISPOSITIVO
                );
        st = conexion.prepareStatement(query);
        Log.e("Participantes",query);
        st.setInt(1, (int) participante.getIdParticipante());
        st.setString(2, participante.getEdad());
        st.setInt(3, (int) participante.getCampania().getIdCampania());
        st.setInt(4, (int) participante.getUbicacion().getIdUbicacion());
        st.setLong(5, dispositivo.getIdDispositivo());
        st.setInt(6, (int) participante.getIdParticipante());
        st.setLong(7, dispositivo.getIdDispositivo());



        return st.executeUpdate() != 0;

    }
}
