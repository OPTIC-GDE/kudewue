package modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import interfaces.Contratos;
import interfaces.Contratos.Ubicaciones;
import sqlite.OperacionesBD;

public class Ubicacion implements Serializable {
    private long idUbicacion;
    private String ubicacion;

    public Ubicacion(long idUbicacion, String ubicacion) {
        this.idUbicacion = idUbicacion;
        this.ubicacion = ubicacion;
    }

    public Ubicacion(long idUbicacion) {
        this.idUbicacion = idUbicacion;
    }



    public boolean persisitirOActualizar(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try{
            datos.actualizarUbicacion(this);
            datos.getWBD().setTransactionSuccessful();
        }catch (SQLiteException e){
            exito = false;
        }finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

    public long getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(long idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    @Override
    public String toString() {
        return ubicacion.trim();
    }

    public static LinkedList<Ubicacion> obtener(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        Cursor cur = datos.obtenerUbicaciones();
        return armarUbicacion(cur);
    }

    private static LinkedList<Ubicacion> armarUbicacion(Cursor cur){
        LinkedList<Ubicacion> ubicaciones = new LinkedList<>();
        while(cur != null && cur.moveToNext()){
            Ubicacion ubicacion = new Ubicacion(cur.getLong(cur.getColumnIndex(Ubicaciones.ID_UBICACION)));
            ubicacion.setUbicacion(cur.getString(cur.getColumnIndex(Ubicaciones.UBICACION)));
            ubicaciones.add(ubicacion);
        }
        return ubicaciones;
    }

    public static LinkedList<Ubicacion> armarUbicacionResultSet(ResultSet cur) throws SQLException {
        LinkedList<Ubicacion> ubicaciones = new LinkedList<>();
        while (cur != null && cur.next()) {
            Ubicacion ubi = new Ubicacion(cur.getLong(Ubicaciones.ID_UBICACION),cur.getString(Ubicaciones.UBICACION));
            ubicaciones.add(ubi);
            Log.e("Ubicacion",ubi.getUbicacion());

        }
        return ubicaciones;

    }

    public static boolean sincronizarUbicaciones(List<Ubicacion> ubicaciones, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (Ubicacion ubicacion : ubicaciones) {
                datos.actualizarUbicacion(ubicacion);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

}
