package modelo;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Path;
import android.util.Log;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import baseDeDatosRemota.Conexion;
import interfaces.Contratos;
import sqlite.OperacionesBD;

public class Dispositivo implements Serializable {
    private long idDispositivo;

    public Dispositivo(long idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public long getIdDispositivo() {
        return idDispositivo;
    }

    public void setIdDispositivo(long idDispositivo) {
        this.idDispositivo = idDispositivo;
    }

    public static Dispositivo obtener(Context context){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        Cursor cursor = datos.obtenerDispositivo();
        return armarDispositivo(cursor);
    }

    private static Dispositivo armarDispositivo(Cursor cur){
        Dispositivo dispositivo = null;
        if(cur != null && cur.moveToNext()){
            dispositivo = new Dispositivo(cur.getLong(cur.getColumnIndex(Contratos.Dispositivos.ID_DISPOSITIVO)));
        }
        return dispositivo;
    }

    public static boolean guardatBaseRemota(Dispositivo dispositivo, Connection conexion) throws SQLException {
        PreparedStatement st;
        String query = String.format("INSERT INTO %s ( %s) SELECT ? WHERE NOT EXISTS ( SELECT * FROM %s WHERE %s = ?) ",
                Contratos.Tablas.DISPOSITIVO,
                Contratos.Dispositivos.ID_DISPOSITIVO,
                Contratos.Tablas.DISPOSITIVO,
                Contratos.Dispositivos.ID_DISPOSITIVO
                );
        Log.e("query",query);
        st = conexion.prepareStatement(query);
        st.setLong(1,dispositivo.getIdDispositivo());
        st.setLong(2,dispositivo.getIdDispositivo());

        return st.executeUpdate() != 0;

    }
}
