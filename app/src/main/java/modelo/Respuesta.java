package modelo;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import ModeloAux.CampaniaPregunta;
import interfaces.Contratos;
import sqlite.OperacionesBD;

public class Respuesta implements Serializable {
    private long idRespuesta;
    private String respuesta;
    private Pregunta pregunta;
    private boolean esCorrecta;

    public Respuesta(long idRespuesta, String respuesta, Pregunta pregunta, boolean esCorrecta) {
        this.idRespuesta = idRespuesta;
        this.respuesta = respuesta;
        this.pregunta = pregunta;
        this.esCorrecta = esCorrecta;
    }

    public Respuesta(long idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public Respuesta(long idRespuesta, String respuesta, Pregunta pregunta) {
        this.idRespuesta = idRespuesta;
        this.respuesta = respuesta;
        this.pregunta = pregunta;
    }

    public long getIdRespuesta() {
        return idRespuesta;
    }

    public void setIdRespuesta(long idRespuesta) {
        this.idRespuesta = idRespuesta;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(String respuesta) {
        this.respuesta = respuesta;
    }

    public Pregunta getPregunta() {
        return pregunta;
    }

    public void setPregunta(Pregunta pregunta) {
        this.pregunta = pregunta;
    }

    public boolean isEsCorrecta() {
        return esCorrecta;
    }

    public void setEsCorrecta(boolean esCorrecta) {
        this.esCorrecta = esCorrecta;
    }

    public static LinkedList<Respuesta> obtenerRespuestas(Context context,Pregunta pregunta){
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        Cursor cur = datos.obtenerRespuestaPregunta(pregunta);
        return armarRespuesta(cur,pregunta);
    }

    private static LinkedList<Respuesta> armarRespuesta(Cursor cur,Pregunta pregunta){
        LinkedList<Respuesta> respuestas = new LinkedList<>();
        while (cur != null && cur.moveToNext()){
            Respuesta res = new Respuesta(cur.getLong(cur.getColumnIndex(Contratos.Respuestas.ID_RESPUESTA)));
            res.setRespuesta(cur.getString(cur.getColumnIndex(Contratos.Respuestas.RESPUESTA)));
            res.setPregunta(pregunta);
            boolean correcta = cur.getInt(cur.getColumnIndex(Contratos.Respuestas.ES_CORRECTA)) != 0;
            res.setEsCorrecta(correcta);
            respuestas.add(res);
        }
        return respuestas;
    }

    public static LinkedList<Respuesta> armarRespuestaResultSet(ResultSet result) throws SQLException {
        LinkedList<Respuesta> respuestas = new LinkedList<>();

        while (result != null && result.next()){

            Respuesta res = new Respuesta(result.getLong(Contratos.Respuestas.ID_RESPUESTA));
            Log.e("Respuesta","obtuvo id respuesta");

            Pregunta preg = new Pregunta(result.getLong(Contratos.Respuestas.ID_PREGUNTA));
            Log.e("Respuesta","obtuvo id pregunta");

            res.setPregunta(preg);
            res.setRespuesta(result.getString(Contratos.Respuestas.RESPUESTA));
            Log.e("Respuesta","obtuvo texto respuesta " + res.getRespuesta());

            boolean correcta = result.getBoolean(Contratos.Respuestas.ES_CORRECTA);
            Log.e("Respuesta","obtuvo si es correcta " + res.getRespuesta());

            res.setEsCorrecta(correcta);
            respuestas.add(res);
            Log.e("Respuesta",res.getRespuesta());
        }
        return respuestas;
    }

    public static boolean sincronizarRespuesta(List<Respuesta> respuestas, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (Respuesta respuesta : respuestas) {
                datos.actualizarRespuesta(respuesta);
                //datos.actualizarCampaniaPregunta(campaniaPregunta);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }
}


