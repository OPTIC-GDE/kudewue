package modelo;


import android.content.Context;
import android.database.sqlite.SQLiteException;
import android.os.Build;
import android.util.Log;

import java.io.Serializable;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

import interfaces.Contratos;
import sqlite.OperacionesBD;

public class Tematica implements Serializable {
    private long idTematica;
    private String tematica;

    public Tematica(long idTematica) {
        this.idTematica = idTematica;
    }

    public Tematica(long idTematica, String tematica) {
        this.idTematica = idTematica;
        this.tematica = tematica;
    }


    public long getIdTematica() {
        return idTematica;
    }

    public void setIdTematica(long idTematica) {
        this.idTematica = idTematica;
    }

    public String getTematica() {
        return tematica;
    }

    public void setTematica(String tematica) {
        this.tematica = tematica;
    }

    @Override
    public String toString() {
        return tematica;

    }

    public boolean persisitirOActualizar(Context context) {
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            datos.actualizarTematica(this);
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != Tematica.class)
            return false;
        Tematica a = (Tematica) o;
        return this.idTematica == a.getIdTematica();
    }

    public static LinkedList<Tematica> armarTematicaResultSet(ResultSet cur) throws SQLException {
        LinkedList<Tematica> tematicas = new LinkedList<>();
        while (cur != null && cur.next()) {
            Tematica tematica = new Tematica(cur.getLong(Contratos.Tematicas.ID_TEMATICA));
            tematica.setTematica(cur.getString(Contratos.Tematicas.NOMBRE));
            tematicas.add(tematica);
            Log.e("Tematica",tematica.getTematica());

        }
        return tematicas;

    }

    public static boolean sincronizarTematicas(List<Tematica> tematicas, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (Tematica tematica : tematicas) {
                datos.actualizarTematica(tematica);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }
}


