package modelo;


import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;

import interfaces.Contratos;
import sqlite.OperacionesBD;

public class Contesta implements Serializable {
    private long idContesta;
    private Participante participante;
    private Respuesta respuesta;
    private String fecha;

    public Contesta(long idContesta, Participante participante, Respuesta respuesta, String fecha) {
        this.idContesta = idContesta;
        this.participante = participante;
        this.respuesta = respuesta;
        this.fecha = fecha;
    }

    public Contesta(Participante participante, Respuesta respuesta, String fecha) {
        this.participante = participante;
        this.respuesta = respuesta;
        this.fecha = fecha;
    }

    public Contesta(long idContesta) {
        this.idContesta = idContesta;
    }

    public long getIdContesta() {
        return idContesta;
    }

    public void setIdContesta(long idContesta) {
        this.idContesta = idContesta;
    }

    public Participante getParticipante() {
        return participante;
    }

    public void setParticipante(Participante participante) {
        this.participante = participante;
    }

    public Respuesta getRespuesta() {
        return respuesta;
    }

    public void setRespuesta(Respuesta respuesta) {
        this.respuesta = respuesta;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public boolean persistir(Context context) {
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        datos.getWBD().beginTransaction();
        boolean exito = true;
        try {
            long id = datos.agregarContesta(this);
           // Log.e("idParticipante2",String.valueOf(id));
           // this.setIdContesta(id);
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;

    }

    private static LinkedList<Contesta> armarContesta(Cursor cur) {
        LinkedList<Contesta> list = new LinkedList<>();
        while (cur.moveToNext()) {
            Contesta contesta = new Contesta(cur.getLong(cur.getColumnIndex(Contratos.Contestan.ID_CONTESTA)));
            Participante par = new Participante(cur.getLong(cur.getColumnIndex(Contratos.Contestan.ID_PARTICIPANTE)));
            Respuesta res = new Respuesta(cur.getLong(cur.getColumnIndex(Contratos.Contestan.ID_RESPUESTA)));

            contesta.setParticipante(par);
            contesta.setRespuesta(res);
            contesta.setFecha(cur.getString(cur.getColumnIndex(Contratos.Contestan.FECHA)));

            list.add(contesta);

        }
        return list;
    }


    public static LinkedList<Contesta> obtenerTodos(Context context) {
        OperacionesBD datos = OperacionesBD.obtenerInstancia(context);
        datos.getRBD().beginTransaction();
        LinkedList<Contesta> retorno;
        try {
            Cursor cur = datos.obtenerContesta();
            retorno = armarContesta(cur);
        } catch (Exception e) {
            retorno = new LinkedList<Contesta>();

        } finally {
            datos.getRBD().endTransaction();
        }
        return retorno;
    }

    public static boolean guardarBaseRemota(Contesta contesta, Connection conexion, Dispositivo dispositivo) throws SQLException, ParseException {
        PreparedStatement st;
       String query = String.format("INSERT INTO %s ( %s, %s, %s, %s,%s) select ?,?,?,?,? WHERE NOT EXISTS " +
                       " (SELECT ( %s,%s, %s, %s,%s) FROM %s WHERE %s = ? AND %s = ?) ",
                Contratos.Tablas.CONTESTA,
               Contratos.Contestan.FECHA,
               Contratos.Contestan.ID_CONTESTA,
               Contratos.Contestan.ID_DISPOSITIVO,
               Contratos.Contestan.ID_RESPUESTA,
               Contratos.Contestan.ID_PARTICIPANTE,
               Contratos.Contestan.FECHA,
               Contratos.Contestan.ID_CONTESTA,
               Contratos.Contestan.ID_DISPOSITIVO,
               Contratos.Contestan.ID_RESPUESTA,
               Contratos.Contestan.ID_PARTICIPANTE,
               Contratos.Tablas.CONTESTA,
               Contratos.Contestan.ID_DISPOSITIVO,
               Contratos.Contestan.ID_CONTESTA
               );
        st = conexion.prepareStatement(query);

        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");

        st.setDate(1, new java.sql.Date(formatter.parse(contesta.getFecha()).getTime()));
        st.setInt(2, (int) contesta.getIdContesta());
        st.setLong(3,  dispositivo.getIdDispositivo());
        st.setInt(4, (int) contesta.getRespuesta().getIdRespuesta());
        st.setInt(5, (int) contesta.getParticipante().getIdParticipante());
        st.setLong(6,  dispositivo.getIdDispositivo());
        st.setInt(7, (int) contesta.getIdContesta());

        Log.e("query contestan",query);

        Log.e("query contestan1",contesta.getFecha());
        Log.e("query contestan2",String.valueOf(contesta.getIdContesta()));
        Log.e("query contestan3",String.valueOf(dispositivo.getIdDispositivo()));
        Log.e("query contestan4",String.valueOf(contesta.getRespuesta().getIdRespuesta()));
        Log.e("query contestan5",String.valueOf(contesta.getParticipante().getIdParticipante()));

        return st.executeUpdate() != 0;

    }
}
