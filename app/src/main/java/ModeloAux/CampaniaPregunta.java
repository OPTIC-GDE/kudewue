package ModeloAux;

import android.content.Context;
import android.database.sqlite.SQLiteException;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import interfaces.Contratos;
import modelo.Pregunta;
import modelo.Tematica;
import sqlite.OperacionesBD;

public class CampaniaPregunta {
    private long campaniaId;
    private long preguntaId;

    public CampaniaPregunta(long campaniaId, long preguntaId) {
        this.campaniaId = campaniaId;
        this.preguntaId = preguntaId;
    }

    public long getCampaniaId() {
        return campaniaId;
    }

    public void setCampaniaId(long campaniaId) {
        this.campaniaId = campaniaId;
    }

    public long getPreguntaId() {
        return preguntaId;
    }

    public void setPreguntaId(long preguntaId) {
        this.preguntaId = preguntaId;
    }

    public static LinkedList<CampaniaPregunta> obtenerCampaniaPregunta(ResultSet cur) throws SQLException {
        LinkedList<CampaniaPregunta> camPreguntas = new LinkedList<>();
        while(cur != null && cur.next()){
            long preId = cur.getLong(Contratos.CampaniasPreguntas.ID_PEGUNTA);
            long camId = cur.getLong(Contratos.CampaniasPreguntas.ID_CAMPANIA);
            camPreguntas.add(new CampaniaPregunta(camId,preId));
        }
        return camPreguntas;

    }

    public static boolean sincronizarCampaniaPregunta(List<CampaniaPregunta> campaniaPreguntas, Context contexto) {
        OperacionesBD datos;
        datos = OperacionesBD.obtenerInstancia(contexto);
        boolean exito = true;
        datos.getWBD().beginTransaction();
        try {
            for (CampaniaPregunta campaniaPregunta : campaniaPreguntas) {
                datos.actualizarCampaniaPregunta(campaniaPregunta);
            }
            datos.getWBD().setTransactionSuccessful();
        } catch (SQLiteException e) {
            exito = false;
        } finally {
            datos.getWBD().endTransaction();
        }
        return exito;
    }
}
