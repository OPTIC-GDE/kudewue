package com.example.kudewue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.Random;

import modelo.Campania;
import modelo.Contesta;
import modelo.Dispositivo;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Ubicacion;

public class ContestaFragment extends Fragment {
    private Pregunta pregunta;
    private LinkedList<Respuesta> respuestas;
    private Respuesta respuestaCorrecta;
    private boolean sonDosPantallas;
    private  Button[] btnRespuestas;
    private Campania campania;
    private Ubicacion ubicacion;
    private Participante participante;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey("pregunta")) {

            pregunta = (Pregunta) getArguments().getSerializable("pregunta");
            respuestas = Respuesta.obtenerRespuestas(this.getContext(),pregunta);
            for (int i = 0; i <  respuestas.size(); i++){
                if (respuestas.get(i).isEsCorrecta()) {
                    respuestaCorrecta = respuestas.get(i);
                }
            }

        }

        if(getArguments().containsKey("sonDosPantallas")){
            sonDosPantallas = getArguments().getBoolean("sonDosPantallas");
        }
        if(getArguments().containsKey("ubicacion")){
            ubicacion = (Ubicacion) getArguments().getSerializable("ubicacion");
        }
        if(getArguments().containsKey("campania")){
            campania = (Campania) getArguments().getSerializable("campania");
        }

        if(getArguments().containsKey("participante")){
            participante = (Participante) getArguments().getSerializable("participante");
            Log.e("idParticipante",String.valueOf(participante.getIdParticipante()));
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_contesta_content, container, false);

        iniciarUI(rootView);
        return rootView;

    }

    private void iniciarUI(View rootView){
        TextView pregunta = rootView.findViewById(R.id.txt_pregunta);


        Button btn1 =  rootView.findViewById(R.id.btn_res1);
        Button btn2 =  rootView.findViewById(R.id.btn_res2);
        Button btn3 =  rootView.findViewById(R.id.btn_res3);

        btnRespuestas = new Button[] {btn1,btn2,btn3};


        pregunta.setText(this.pregunta.getPregunta().trim());
        if(respuestas.isEmpty()){
            respuestas.add(new Respuesta(1,"blanco",this.pregunta,true));
            respuestas.add(new Respuesta(2,"negro",this.pregunta,false));
            respuestas.add(new Respuesta(3,"rosa",this.pregunta,false));

        }
        Collections.shuffle(respuestas);

        for (int i = 0; i < 3;i++){
            final Respuesta res = respuestas.get(i);
            btnRespuestas[i].setText(res.getRespuesta().trim());
            if(respuestas.get(i).isEsCorrecta()){
                btnRespuestas[i].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = new Date();
                        Contesta contesta = new Contesta(participante, res ,(formatter.format(date)));
                        contesta.persistir(getContext());
                        respuestaCorrecta();
                    }
                });
            }else{
                btnRespuestas[i].setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
                        Date date = new Date();
                        Contesta contesta = new Contesta(participante, res ,(formatter.format(date)));
                        contesta.persistir(getContext());
                        respuestaIncorrecta();
                    }
                });
            }
        }


    }

    public void respuestaCorrecta(){
        new AlertDialog.Builder(this.getContext())
                .setTitle("Correcto")
                .setMessage("Bien!! La respuesta es correcta")
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cambioDePantallas();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    public void respuestaIncorrecta(){
        new AlertDialog.Builder(this.getContext())
                .setTitle("Incorrecto")
                .setMessage(":( la respuesta no es correcta")
                .setPositiveButton("Continuar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        cambioDePantallas();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_info)
                .show();
    }

    private void cambioDePantallas(){
        if(sonDosPantallas){
            Intent intent = new Intent(getActivity(), RuedaActivity.class);
            intent.putExtra("campania",campania);
            intent.putExtra("ubicacion",ubicacion);
            startActivity(intent);
        }else{
            for (Button btn: btnRespuestas){
                btn.setClickable(false);
            }
        }
    }
}
