package com.example.kudewue;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.adefruandta.spinningwheel.SpinningWheelView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Random;
import java.util.logging.Logger;

import modelo.Campania;
import modelo.Dispositivo;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Tematica;
import modelo.Ubicacion;

public class RuedaActivity extends AppCompatActivity implements SpinningWheelView.OnRotationListener<Tematica> {

    private SpinningWheelView wheelView;
    private boolean sonDosPantallas;
    private Button rotate;
    private Campania campania;
    private LinkedList<Tematica> tematicas;
    private Ubicacion ubicacion;
    private Participante participante;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rueda);

        wheelView = findViewById(R.id.wheel);
        rotate = findViewById(R.id.rotate);




        campania = (Campania) getIntent().getSerializableExtra("campania");
        ubicacion = (Ubicacion) getIntent().getSerializableExtra("ubicacion");

        tematicas = obtenerTematicas();

        if(tematicas.isEmpty()){
            tematicas.add(new Tematica(1, "historia"));
            tematicas.add(new Tematica(2, "filosofia"));
            tematicas.add(new Tematica(3, "geologia"));
            tematicas.add(new Tematica(4, "matematica"));
            tematicas.add(new Tematica(5, "fisica"));
        }

        participante = new Participante(campania, Dispositivo.obtener(this.getBaseContext()),ubicacion,true,"0");
        participante.persistir(this.getBaseContext());

        wheelView.setItems(tematicas);
        wheelView.setOnRotationListener(this);

        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rnd = new Random();
                int maxAngle = (int) (rnd.nextDouble() * 30 + 70);
                int duration = (int) (rnd.nextDouble() * 1000 + 4000);
                int interval = (int) (rnd.nextDouble() * 30 + 50);
                wheelView.rotate(maxAngle, duration, interval);
            }
        });

        if(findViewById(R.id.contesta_content) != null){
            sonDosPantallas = false;
            Log.e("tag","son dos pantallas");
        }else{
            Log.e("tag","no son dos pantallas");
            sonDosPantallas = true;
        }


    }
    private LinkedList<Pregunta> obtenerPreguntas(Tematica item){
        LinkedList<Pregunta> preguntas = (LinkedList<Pregunta>) campania.getPreguntas(this.getApplicationContext());
        LinkedList<Pregunta> preguntasAux = new LinkedList<>();

        for (int i = 0; i < preguntas.size(); i++){
            if(preguntas.get(i).getTematica().getIdTematica() == item.getIdTematica()){
                preguntasAux.add(preguntas.get(i));
            }
        }
        return preguntasAux;
    }

    private LinkedList<Tematica> obtenerTematicas(){
        final HashMap<Long,Tematica> tematicas = new HashMap<>();
        LinkedList<Pregunta> preguntas = (LinkedList<Pregunta>) campania.getPreguntas(this.getApplicationContext());
        for (int i = 0; i < preguntas.size(); i++){
            tematicas.put(preguntas.get(i).getTematica().getIdTematica(),preguntas.get(i).getTematica());
        }
        return new LinkedList<Tematica>(tematicas.values());

    }


    @Override
    public void onRotation() {
    }

    @Override
    public void onStopRotation(Tematica item) {

        LinkedList<Pregunta> preguntas =  obtenerPreguntas(item);
        int tam = preguntas.size();
        Random rnd = new Random();
        if(preguntas.size() < 1 ){
            preguntas.add(new Pregunta(1,"De que color es el caballo blanco de san martin",tematicas.get(0)));
            preguntas.add(new Pregunta(2,"San martin En que anio conquisto marte?",tematicas.get(0)));


        }
        int element =  rnd.nextInt( (preguntas.size() - 1)  + 1);

        Pregunta pregunta = preguntas.get(element);
        if(sonDosPantallas){
            Intent intent = new Intent(getApplicationContext(),ContestaActivity.class);
            intent.putExtra("participante",participante);
            intent.putExtra("pregunta",pregunta);
            intent.putExtra("campania",campania);
            intent.putExtra("ubicacion",ubicacion);

            startActivity(intent);
        }else{
            Bundle arguments = new Bundle();
            ContestaFragment fragment = new ContestaFragment();
            arguments.putSerializable("participante",participante);
            arguments.putSerializable("pregunta",pregunta);
            arguments.putSerializable("campania",campania);
            arguments.putSerializable("ubicacion",ubicacion);

            arguments.putBoolean("sonDosPantallas",false);
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.contesta_content, fragment)
                    .commit();
        }
        Toast.makeText(this, item.toString(), Toast.LENGTH_LONG).show();
    }
}
