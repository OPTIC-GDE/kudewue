package com.example.kudewue;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.material.appbar.CollapsingToolbarLayout;

import java.util.ArrayList;

import modelo.Campania;
import modelo.Ubicacion;

public class UbicacionFragment extends Fragment {
    private Ubicacion ubicacion;
    private Campania campania;

    public UbicacionFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments().containsKey("campania")) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            campania = (Campania) getArguments().getSerializable("campania");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_ubicacion, container, false);

        iniciarUI(rootView);
        return rootView;

    }

    public void iniciarUI(View rootView) {
        if (campania != null) {
            Log.e("mensaje", "no es null");

            TextView tx = rootView.findViewById(R.id.txt_descripcion_campania);
            tx.setText(campania.getDescripcion());
            ((TextView) rootView.findViewById(R.id.txt_campania_nombre)).setText(campania.getNombre());

        }



    }
}
