package com.example.kudewue;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Spinner;

import java.util.LinkedList;

import baseDeDatosRemota.ObtencionDatosRemotos;
import modelo.Campania;
import modelo.Contesta;
import modelo.Dispositivo;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Tematica;
import modelo.Ubicacion;
import utiles.OnTaskComplete;

public class MenuPrincipal extends AppCompatActivity implements OnTaskComplete {
    private boolean sonDosPantallas;
    private ProgressBar sincronizando;
    private LinkedList<Campania> campanias;
    private LinkedList<Ubicacion> ubicaciones;
    private Campania campaniaSeleccionda;
    private Ubicacion ubicacionSeleccionada;
    ArrayAdapter<Campania> adapterCampania;
    ArrayAdapter<Ubicacion> adapterUbicacion;
    Spinner spnCampanias;
    Spinner spnUbicacion;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal);

        Button btnJugar = findViewById(R.id.btn_jugar);
        Button btnSincronizar = findViewById(R.id.btn_sincronizar);
        testBD();

        spnCampanias = findViewById(R.id.spn_campania);
        spnUbicacion= findViewById(R.id.spn_ubicacion);

        if(findViewById(R.id.ubicacion_detail_container) != null){
            sonDosPantallas = true;
        }else{
            sonDosPantallas = false;
        }
        ubicaciones = Ubicacion.obtener(this.getBaseContext());
        campanias = Campania.obtener(this.getBaseContext());

        if(!campanias.isEmpty()){
            campaniaSeleccionda = campanias.getFirst();

        }

        if(!ubicaciones.isEmpty()){
            ubicacionSeleccionada = ubicaciones.getFirst();
        }

        adapterCampania =
                new ArrayAdapter<>(this.getBaseContext(), android.R.layout.simple_spinner_dropdown_item, campanias);
        spnCampanias.setAdapter(adapterCampania);

        adapterUbicacion =
                new ArrayAdapter<>(this.getBaseContext(), android.R.layout.simple_spinner_dropdown_item, ubicaciones);
        spnUbicacion.setAdapter(adapterUbicacion);



        spnCampanias.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                campaniaSeleccionda = campanias.get(i);
                mostrarDetalleCampania(campanias.get(i));
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        spnUbicacion.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                ubicacionSeleccionada = ubicaciones.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        sincronizando = findViewById(R.id.progressBar);

        btnSincronizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sincronizar();
            }
        });

        btnJugar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(campanias.isEmpty() || ubicaciones.isEmpty()){
                    crearDialogo("Porfavor sincronize la aplicacion","Jugar");
                }else{
                    Intent intent = new Intent(getApplicationContext(),RuedaActivity.class);
                    intent.putExtra("campania",campaniaSeleccionda);
                    intent.putExtra("ubicacion",ubicacionSeleccionada);
                    startActivity(intent);
                }

            }
        });

    }

    private void mostrarDetalleCampania(Campania campania){
        if(sonDosPantallas) {
            Bundle arguments = new Bundle();
            arguments.putSerializable("campania", campania);
            UbicacionFragment fragment = new UbicacionFragment();
            fragment.setArguments(arguments);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.ubicacion_detail_container, fragment)
                    .commit();
        }
    }

    public void testBD(){
        Ubicacion ubicacion = new Ubicacion(1,"san agustin");
        ubicacion.persisitirOActualizar(this.getApplicationContext());
        Tematica tematica = new Tematica(1,"matematica");
        tematica.persisitirOActualizar(this.getApplicationContext());
        Pregunta pregunta = new Pregunta(1,"Quien gano el mundial",tematica);
        pregunta.persisitirOActualizar(this.getApplicationContext());
        LinkedList<Pregunta> preguntas = new LinkedList<>();
        preguntas.add(pregunta);
       // Campania campania = new Campania(1,"Campania por la salud","Esta campania tiene como plan estudiar el conocimiebto de la poblacion en material de salud, tanto como en la educacion sexual como la higiente",true,preguntas);
    //    campania.persisitirOActualizar(getApplicationContext());
     //   Participante participante = new Participante(1,campania, Dispositivo.obtener(getApplicationContext()),ubicacion,true,"32");
      //  participante.persistir(getApplicationContext());

    //   Respuesta res = new Respuesta(1,"si",pregunta,true);
     //   Contesta con = new Contesta(1,participante,res,"1234213");
   //     con.persistir(getApplicationContext());

    }

    private void sincronizar(){
        sincronizando.setVisibility(View.VISIBLE);
        ObtencionDatosRemotos task = new ObtencionDatosRemotos(this,this.getApplicationContext());
        task.execute();
    }

    @Override
    public boolean onTaskCompleted(boolean respuesta) {
        sincronizando.setVisibility(View.GONE);

        if(respuesta){
            crearDialogo("Los datos se han sincronizado con exito","Sincronizador");
        }else{
            crearDialogo("Ubo un problema al sincronizar los datos porfavor cheque su conexion a internet","Sincronizador");
        }
        ubicaciones = Ubicacion.obtener(this.getBaseContext());
        campanias = Campania.obtener(this.getBaseContext());
        adapterCampania =
                new ArrayAdapter<>(this.getBaseContext(), android.R.layout.simple_spinner_dropdown_item, campanias);
        spnCampanias.setAdapter(adapterCampania);

        adapterUbicacion =
                new ArrayAdapter<>(this.getBaseContext(), android.R.layout.simple_spinner_dropdown_item, ubicaciones);
        spnUbicacion.setAdapter(adapterUbicacion);
        return respuesta;
    }

    public void crearDialogo(String mensaje,String titulo){
        AlertDialog.Builder builder = new AlertDialog.Builder(MenuPrincipal.this);
        builder.setTitle(titulo);
        builder.setMessage(mensaje);
        builder.setPositiveButton("Cerrar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });

        builder.show();
    }
}
