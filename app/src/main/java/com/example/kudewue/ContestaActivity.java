package com.example.kudewue;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;


import modelo.Campania;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Ubicacion;

public class ContestaActivity extends AppCompatActivity {
    private Pregunta pregunta;
    private Participante participante;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pregunta_activity);

        Ubicacion ubicacion =  (Ubicacion) getIntent().getSerializableExtra("ubicacion");
        pregunta = (Pregunta) getIntent().getSerializableExtra("pregunta");
        Campania cam = (Campania) getIntent().getSerializableExtra("campania");
        participante = (Participante) getIntent().getSerializableExtra("participante");

        Bundle arguments = new Bundle();
        ContestaFragment fragment = new ContestaFragment();

        arguments.putSerializable("pregunta",pregunta);
        arguments.putSerializable("campania",cam);
        arguments.putBoolean("sonDosPantallas",true);
        arguments.putSerializable("ubicacion",ubicacion);
        arguments.putSerializable("participante",participante);
        fragment.setArguments(arguments);
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contesta_content, fragment)
                .commit();

    }

    private void cargarElementos(){

    }
}
