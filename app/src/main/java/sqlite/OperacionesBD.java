package sqlite;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;

import com.google.android.material.tabs.TabLayout;

import ModeloAux.CampaniaPregunta;
import interfaces.Contratos.*;
import modelo.Campania;
import modelo.Contesta;
import modelo.Participante;
import modelo.Pregunta;
import modelo.Respuesta;
import modelo.Tematica;
import modelo.Ubicacion;

public final class OperacionesBD {

    private static KudewueBD baseDatos;
    //esta clase se encarga de hacer loa accesos finales a la base de datos
    private static OperacionesBD instancia = new OperacionesBD();

    private OperacionesBD() {

    }

    //Funcion singleton para la base de datos
    public static OperacionesBD obtenerInstancia(Context contexto) {
        if (baseDatos == null) baseDatos = new KudewueBD(contexto);
        return instancia;
    }

    public Cursor obtenerCampaniasActivas() {
        SQLiteDatabase db = baseDatos.getReadableDatabase();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tablas = Tablas.CAMPANIA;
        String[] proyeccion = {
                Campanias.ID_CAMPANIA,
                Campanias.NOMBRE,
                Campanias.DESCRIPCION
        };
        String where = Campanias.ES_ACTIVA + " = " + 1;
        builder.setTables(tablas);
        Cursor resultado = builder.query(db, proyeccion, where, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerUbicaciones() {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tablas = Tablas.UBICACION;
        String[] proyeccion = {
                Ubicaciones.ID_UBICACION,
                Ubicaciones.UBICACION
        };
        builder.setTables(tablas);
        Cursor resultado = builder.query(db, proyeccion, null, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerParticipantes(){
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tablas = Tablas.PARTICIPANTE;

        String[] proyeccion = {
                Participantes.ID_PARTICIPANTE,
                Participantes.EDAD,
                Participantes.ID_UBICACION,
                Participantes.ID_DISPOSITIVO,
                Participantes.ES_ACTIVO,
                Participantes.ID_CAMPANIA
        };
        builder.setTables(tablas);
        Cursor resultado = builder.query(db, proyeccion, null, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerPreguntas() {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tablas = Tablas.PREGUNTA;
        String[] proyeccion = {
                Preguntas.ID_PREGUNTA,
                Preguntas.ID_TEMATICA,
                Preguntas.PREGUNTA
        };
        builder.setTables(tablas);
        Cursor resultado = builder.query(db, proyeccion, null, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerRespuestaPregunta(Pregunta pregunta) {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tabla = Tablas.RESPUESTA;

        String[] proyeccion = {
                Respuestas.ID_RESPUESTA,
                Respuestas.ES_CORRECTA,
                Respuestas.RESPUESTA
        };
        String where = Respuestas.ID_PREGUNTA + " = " + pregunta.getIdPregunta();
        builder.setTables(tabla);
        Cursor res = builder.query(db,proyeccion,where,null, null, null, null);
        return res;

    }

    public Cursor obtenerParticipanteActivo() {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String a = Tablas.CAMPANIA;
        String b = Tablas.UBICACION;
        String c = Tablas.PARTICIPANTE;

        String tabla = String.format("%s INNER JOIN %s ON %s = %s INNER JOIN %s ON %s = %s",
                a,
                c,
                a + "." + Campanias.ID_CAMPANIA,
                c + "." + Participantes.ID_CAMPANIA,
                b,
                c + "." + Participantes.ID_UBICACION,
                b + "." + Ubicaciones.ID_UBICACION);
        String[] proyeccion = {
                Participantes.ID_PARTICIPANTE,
                Participantes.EDAD,
                Participantes.ES_ACTIVO,
                a + "." + Campanias.ID_CAMPANIA,
                a + "." + Campanias.DESCRIPCION,
                a + "." + Campanias.NOMBRE,
                a + "." + Campanias.ES_ACTIVA,
                b + "." + Ubicaciones.ID_UBICACION,
                b + "." + Ubicaciones.UBICACION
        };
        // Te trae el participante activo solo si la camapania esta activa
        String where = Participantes.ES_ACTIVO + " = 1 and " + Campanias.ES_ACTIVA + " = 1";
        builder.setTables(tabla);
        Cursor resultado = builder.query(db, proyeccion, where, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerDispositivo() {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        String tabla = Tablas.DISPOSITIVO;
        builder.setTables(tabla);
        Cursor resultado = builder.query(db, null, null, null, null, null, null);
        return resultado;
    }

    public Cursor obtenerCampaniaPreguntas(Campania campania) {
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        // tablas
        String a = Tablas.PREGUNTA;
        String c = Tablas.CAMPANIA_PREGUNTA;
        String d = Tablas.TEMATICA;
        //atributos identificados con su tabla para evitar ambiguedades
        String atributoA = a + "." + Preguntas.ID_PREGUNTA;
        String atributoA2 = a + "." + Preguntas.ID_TEMATICA;
        String atributoD = d + "." + Tematicas.ID_TEMATICA;
        String atributoC = c + "." + CampaniasPreguntas.ID_PEGUNTA;
        String atributoC2 = c + "." + CampaniasPreguntas.ID_CAMPANIA;
        //where
        String where = atributoC2 + " = " + campania.getIdCampania();
        //Tabla final
        String[] proyeccion = {
                a + "." + Preguntas.ID_PREGUNTA,
                a + "." + Preguntas.PREGUNTA,
                d + "." + Tematicas.ID_TEMATICA,
                d + "." + Tematicas.NOMBRE

        };
        String formatQuery = "%s INNER JOIN %s ON %s = %s INNER JOIN %s ON %s = %s";
        formatQuery = String.format(formatQuery,
                a,
                c,
                atributoA, atributoC,
                d,
                atributoA2, atributoD
        );

        builder.setTables(formatQuery);
        Cursor resultado = builder.query(db, proyeccion, where, null, null, null, atributoC2 + " , " + atributoC);
        return resultado;
    }

    public Cursor obtenerContesta(){
        SQLiteDatabase db = getRBD();
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();

        String c = Tablas.CONTESTA;

        String[] proyeccion = {
                c + "." + Contestan.ID_CONTESTA,
                c + "." + Contestan.FECHA,
                c + "." + Contestan.ID_DISPOSITIVO,
                c + "." + Contestan.ID_PARTICIPANTE,
                c + "." + Contestan.ID_RESPUESTA,
        };
        builder.setTables(c);
        Cursor resultado = builder.query(db,proyeccion,null,null,null,null,null);
        return  resultado;



    }

    public long agregarParticipante(Participante participante) {
        ContentValues content = new ContentValues();
        //content.put(Participantes.ID_PARTICIPANTE,participante.getIdParticipante());
        content.put(Participantes.EDAD, participante.getEdad());
        content.put(Participantes.ES_ACTIVO, participante.isEsActivo());
        content.put(Participantes.ID_CAMPANIA, participante.getCampania().getIdCampania());
        content.put(Participantes.ID_DISPOSITIVO, participante.getDispositivo().getIdDispositivo());
        content.put(Participantes.ID_UBICACION, participante.getUbicacion().getIdUbicacion());
        return getWBD().insertOrThrow(Tablas.PARTICIPANTE, null, content);
    }

    public long agregarContesta(Contesta contesta) {
        ContentValues content = new ContentValues();
        content.put(Contestan.FECHA, contesta.getFecha());
        content.put(Contestan.ID_DISPOSITIVO, contesta.getParticipante().getDispositivo().getIdDispositivo());
        content.put(Contestan.ID_PARTICIPANTE, contesta.getParticipante().getIdParticipante());
        content.put(Contestan.ID_RESPUESTA, contesta.getRespuesta().getIdRespuesta());
        long idContesta = getWBD().replace(Tablas.CONTESTA, null, content);
        return idContesta;
    }

    public long actualizarTematica(Tematica tematica) {
        ContentValues content = new ContentValues();
        content.put(Tematicas.ID_TEMATICA,tematica.getIdTematica());
        content.put(Tematicas.NOMBRE,tematica.getTematica());
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        //Retorna -1 si hay un error
        return db.replace(Tablas.TEMATICA, null, content);

    }
    
    public long actualizarCampania(Campania campania) {
        ContentValues content = new ContentValues();
        content.put(Campanias.ID_CAMPANIA, campania.getIdCampania());
        content.put(Campanias.DESCRIPCION, campania.getDescripcion());
        content.put(Campanias.NOMBRE, campania.getNombre());
        int activa = 0;
        if (campania.isActiva()) activa = 1;
        content.put(Campanias.ES_ACTIVA, activa);
        SQLiteDatabase db = baseDatos.getWritableDatabase();
        //Retorna -1 si hay un error
        if(campania.getPreguntas() != null){
            actualizarCampaniaPregunta(campania);

        }
        return db.replace(Tablas.CAMPANIA, null, content);

    }

    public long actualizarUbicacion(Ubicacion ubicacion) {
        ContentValues content = new ContentValues();
        content.put(Ubicaciones.ID_UBICACION, ubicacion.getIdUbicacion());
        content.put(Ubicaciones.UBICACION, ubicacion.getUbicacion());
        SQLiteDatabase db = getWBD();
        return db.replace(Tablas.UBICACION, null, content);
    }

    public long actualizarPregunta(Pregunta pregunta) {
        ContentValues content = new ContentValues();
        content.put(Preguntas.ID_PREGUNTA, pregunta.getIdPregunta());
        content.put(Preguntas.ID_TEMATICA, pregunta.getTematica().getIdTematica());
        content.put(Preguntas.PREGUNTA, pregunta.getPregunta());
        SQLiteDatabase db = getWBD();
        return db.replace(Tablas.PREGUNTA, null, content);
    }

    private void actualizarCampaniaPregunta(Campania cam){
        for (Pregunta pregunta:cam.getPreguntas()) {
            ContentValues content = new ContentValues();
            content.put(Campanias.ID_CAMPANIA,cam.getIdCampania());
            content.put(Preguntas.ID_PREGUNTA,pregunta.getIdPregunta());
            getWBD().replace(Tablas.CAMPANIA_PREGUNTA,null,content);
        }

    }

    public void actualizarCampaniaPregunta(CampaniaPregunta cam){
            ContentValues content = new ContentValues();
            content.put(Campanias.ID_CAMPANIA,cam.getCampaniaId());
            content.put(Preguntas.ID_PREGUNTA,cam.getPreguntaId());
            getWBD().replace(Tablas.CAMPANIA_PREGUNTA,null,content);

    }

    public void actualizarRespuesta(Respuesta res){
        ContentValues content = new ContentValues();
        content.put(Respuestas.ID_PREGUNTA,res.getPregunta().getIdPregunta());
        content.put(Respuestas.ID_RESPUESTA,res.getIdRespuesta());
        if(res.isEsCorrecta()){
            content.put(Respuestas.ES_CORRECTA,1);

        }else{
            content.put(Respuestas.ES_CORRECTA,0);

        }
        content.put(Respuestas.RESPUESTA, res.getRespuesta());
        getWBD().replace(Tablas.RESPUESTA,null,content);


    }
    //obtener base de datos de escritura
    public SQLiteDatabase getWBD() {
        return baseDatos.getWritableDatabase();
    }

    public SQLiteDatabase getRBD() {
        return baseDatos.getReadableDatabase();
    }

}
