package sqlite;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.awt.font.TextAttribute;
import java.util.Random;

import interfaces.Contratos.*;


public class KudewueBD extends SQLiteOpenHelper {
    private static final String NOMBRE_BASE_DATOS = "KudewueBD.sqlite";

    private static final int VERSION_ACTUAL = 1;

    private final Context contexto;


    public KudewueBD(Context context) {
        super(context, NOMBRE_BASE_DATOS, null, VERSION_ACTUAL);
        this.contexto = context;
    }



    @Override
    public void onCreate(SQLiteDatabase db) {
//=========================================================================================
//                     CAMPANIA
//=========================================================================================
        final String formatCampania = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT, %s TEXT,%s INTEGER)";
        db.execSQL(String.format(formatCampania,
                Tablas.CAMPANIA,
                Campanias.ID_CAMPANIA,
                Campanias.DESCRIPCION,
                Campanias.NOMBRE,
                Campanias.ES_ACTIVA));
//=========================================================================================
//                     DISPOSITIVO
//=========================================================================================

        final String formatDispositivo = "CREATE TABLE %s (%s INTEGER PRIMARY KEY)";
        db.execSQL(String.format(formatDispositivo,
                Tablas.DISPOSITIVO,
                Dispositivos.ID_DISPOSITIVO));
//=========================================================================================
//                     UBICACION
//=========================================================================================
        final String formatUbicacion = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,%s TEXT)";
        db.execSQL(String.format(formatUbicacion,
                Tablas.UBICACION,
                Ubicaciones.ID_UBICACION,
                Ubicaciones.UBICACION));

//=========================================================================================
//                     TEMATICA
//=========================================================================================

        final String formatTematica = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT)";
        db.execSQL(String.format(formatTematica,
                Tablas.TEMATICA,
                Tematicas.ID_TEMATICA,
                Tematicas.NOMBRE));

//=========================================================================================
//                     PREGUNTA
//=========================================================================================

        final String formatPregunta = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT,%s INTEGER %s)";
        db.execSQL(String.format(formatPregunta,
                Tablas.PREGUNTA,
                Preguntas.ID_PREGUNTA,
                Preguntas.PREGUNTA,
                Preguntas.ID_TEMATICA,Referencia.ID_TEMATICA
        ));
//=========================================================================================
//                     PARTICIPANTE
//=========================================================================================

        final String formatParticipante = "CREATE TABLE %s " +
                "(%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER %s, %s INTEGER %s,%s INTEGER %s, %s INTEGER,%s INTEGER)";
        db.execSQL(String.format(formatParticipante,
                Tablas.PARTICIPANTE,
                Participantes.ID_PARTICIPANTE,
                Participantes.ID_UBICACION,Referencia.ID_UBICACION,
                Participantes.ID_DISPOSITIVO,Referencia.ID_DISPOSITIVO,
                Participantes.ID_CAMPANIA,Referencia.ID_CAMPANIA,
                Participantes.ES_ACTIVO,
                Participantes.EDAD));

//=========================================================================================
//                     RESPUESTA
//=========================================================================================
        final String formatRespuesta = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT, %s TEXT, %s INTEGER, %s INTEGER %s)";
        db.execSQL(String.format(formatRespuesta,
                Tablas.RESPUESTA,
                Respuestas.ID_RESPUESTA,
                Respuestas.RESPUESTA,
                Respuestas.ES_CORRECTA,
                Respuestas.ID_PREGUNTA, Referencia.ID_PREGUNTA));

//=========================================================================================
//                     CONTESTA
//=========================================================================================

        final String formatContesta = "CREATE TABLE %s (%s INTEGER PRIMARY KEY AUTOINCREMENT,%s INTEGER %s, %s INTEGER %s,%s INTEGER %s ,%s TEXT)";
        db.execSQL(String.format(formatContesta,
                Tablas.CONTESTA,
                Contestan.ID_CONTESTA,
                Contestan.ID_DISPOSITIVO, Referencia.ID_DISPOSITIVO,
                Contestan.ID_PARTICIPANTE, Referencia.ID_PARTICIPANTE,
                Contestan.ID_RESPUESTA,Referencia.ID_RESPUESTA,
                Contestan.FECHA));


//=========================================================================================
//                     CAMPANIA_PREGUNTA
//=========================================================================================
        final String formatCampaniaPregunta = "CREATE TABLE %s(%s INTEGER %s,%s INTEGER %s,PRIMARY KEY (%s,%s))";
        db.execSQL(String.format(formatCampaniaPregunta,
                Tablas.CAMPANIA_PREGUNTA,
                CampaniasPreguntas.ID_CAMPANIA,Referencia.ID_CAMPANIA,
                CampaniasPreguntas.ID_PEGUNTA,Referencia.ID_PREGUNTA,
                CampaniasPreguntas.ID_PEGUNTA,CampaniasPreguntas.ID_CAMPANIA));
//=========================================================================================
//                     GENERACION DE NOMBRE DEL DISPOSITIVO ALEATORIO
//=========================================================================================
        Random rand = new Random();
        long n = rand.nextLong();
        String insertDispostivo = String.format("INSERT INTO %s ( %s ) VALUES (%s)",
                Tablas.DISPOSITIVO,
                Dispositivos.ID_DISPOSITIVO,
                n);
        db.execSQL(insertDispostivo);

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
